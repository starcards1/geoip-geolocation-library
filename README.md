# GeoIP - Geolocation and Geocoding library


## 1. Get your own current location
If executed in a users browser or using AJAX, it will autodetect your IP address and convert it to your current location

### FIELDS RETURNED
```lat, lng, city, state, zip, country, areacode```

### ENDPOINT URI
```http://terrawire.com:8080```

## 2. Convert IP Adress to Location
Provide the endpoint an IP adress and it will convert it to a location

### FIELDS RETURNED
lat, lng, city, state, zip, country, areacode

### ENDPOINT URI
```http://terrawire.com:8080/json/USER_IP_ADDRESS```





