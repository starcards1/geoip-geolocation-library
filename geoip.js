 "use strict";
process.title = 'GeoIP-Server';
var server, APIServer, SMSServer
var json;
var express=require('express');
var results, fields
var express
var app, mysql,pool
var gatewayPort = 8080
var fetch = require('node-fetch');
var http
var res,rq
var referrer
    var exec = require('child_process').exec;
	var maxmind = require('maxmind');
	http = require('http');
	server = http.createServer(function(request, response){
		referrer=request.headers.referer
		if (referrer=='undefined') referrer=request.header('Referer');
		if ((referrer=='undefined') || (!referrer))
			referrer=pathExtractor(request)
	});
	express=require('express');
	app = express();
	app.use(express.static('public'));

	var server = app.listen(gatewayPort, function () {
	  console.log("sms engine listening at Port " + gatewayPort)
	})
	app.get('/', function (req, res) {
		rq=req
		var ip=req.connection.remoteAddress
		if (ip) var loc=nlookup(req.ip)
		res.end(JSON.stringify(loc))
		console.log (loc)
		return;
	})
	app.get('/json', function (req, res) {
		rq=req
		var ip=req.connection.remoteAddress
		if (ip) var loc=nlookup(req,ip)
		res.end(JSON.stringify(loc))
		 console.log (loc)
	})
	app.get('/json/:ip', function (req, res) {
		rq=req
		var ip=req.params.ip
		if (ip==''){
			ip=req.connection.remoteAddress
		}
		if (ip) var loc=nlookup(req,ip)
		res.end(JSON.stringify(loc))
		console.log (loc)
		return
	})
	
	app.get('/:ip', function (req, res) {
		rq=req
		var ip=req.params.ip
		if (ip==''){
			ip=req.connection.remoteAddress
		}
		if (ip)	var loc=nlookup(req,ip)
		res.end(JSON.stringify(loc))
		console.log (loc)
		return
	})
	
	function nlookup(req,ip) {
		var country,city,state,zip,lat,lng
		if (!ip) return false
		var opts = {
		  watchForUpdates: true,
		  watchForUpdatesHook: () => { console.log('database updated!'); },
		};
		var lookup = maxmind.open('/usr/share/GeoIP/GeoLite2-City.mmdb', opts);
		var geo = lookup.get(ip);
		if (!geo) return false
		if (!geo.location) {
			lat='[NA]'
			lng='[NA]'
		} else {
			lat=geo.location.latitude
			lng=geo.location.longitude
		}
		if (geo.country) country=((geo.country.names).en)
			else country='[NA]'
		if (geo.city) city=((geo.city.names).en)
			else city='[NA]'
		if (geo.postal) zip=geo.postal.code
			else zip='[NA]'
		if (geo.subdivisions) {
			if (geo.subdivisions[0]) {
				state=geo.subdivisions[0].iso_code;
			}
		} else {
			state='[NA]'
		}
		var loc=[{city:city,state:state,zip:zip,lat:lat,lng:lng,ip:ip.replace('::ffff:',''),country:country}]
		return loc
	}

	function fetchURL(url) {
		exec(url, function (error, stdout, stderr) { 
		  res.end(stdout);
		  console.log('stderr: ' + stderr);
		  if (error !== null) {
			console.log('exec error: ' + error);
		  }
		});
	}

	function pathExtractor(req) {
	  function escapeRegExp(str) {
	   return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
	  }
	  // Replace utility function
	  function replaceAll(str, find, replace) {
	   return str.replace(new RegExp(escapeRegExp(find), 'g'), replace); 
	  }
	  return replaceAll(req.get('referer'), req.get('origin'), '');
	}